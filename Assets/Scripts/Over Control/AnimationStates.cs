﻿public class AnimationStates {
	public static string PLAYER_RUN = "Run";
	public static string PLAYER_ATTACK = "Attack";
	public static string PLAYER_SKILL_1 = "Skill1";
	public static string PLAYER_SKILL_2 = "Skill2";
	public static string PLAYER_SKILL_3 = "Skill3";
	public static string PLAYER_DEAD = "Dead";
	public static string PLAYER_VICTORY = "Victory";
	public static string PLAYER_BASE_LAYER_DYING = "Base Layer.Dying";
	public static string PLAYER_BASE_LAYER_VICTORY = "Base Layer.Victory";

	public static string ENEMY_ATTACK = "Attack";
	public static string ENEMY_RUN = "Run";
	public static string ENEMY_SPEED = "Speed";
	public static string ENEMY_VICTORY = "Victory";
	public static string ENEMY_BASE_LAYER_STAND = "Base Layer.Stand";

	public static string ENEMY_BE_ATTACKED = "BeAttacked";
	public static string ENEMY_DEAD = "Dead";
	public static string ENEMY_BASE_LAYER_DYING = "Base Layer.Dying";
	public static string ENEMY_BASE_LAYER_HIT = "Base Layer.Hit";

	public static string BOSS_SKILL_1 = "Skill1";
	public static string BOSS_SKILL_2 = "Skill2";
	public static string BOSS_SKILL_3 = "Skill3";
	public static string BOSS_WALK = "Walk";
	public static string BOSS_DEAD = "Dead";
	public static string BOSS_BASE_LAYER_DEAD = "Base Layer.Dead";


	public static string BOSS_ATTACK = "Attack";
	public static string BOSS_RUN = "Run";
	public static string BOSS_SPEED = "Speed";
	public static string BOSS_VICTORY = "Victory";
	public static string BOSS_BASE_LAYER_STAND = "Base Layer.Stand";
}
