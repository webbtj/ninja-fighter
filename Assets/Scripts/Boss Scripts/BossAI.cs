﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAI : MonoBehaviour {

	private Animator animator;
	private Transform playerTransform;
	private PlayerHealth playerHealth;
	private BossHealth bossHealth;

	private string ANIMATION_SKILL_1 = AnimationStates.BOSS_SKILL_1;
	private string ANIMATION_SKILL_2 = AnimationStates.BOSS_SKILL_2;
	private string ANIMATION_SKILL_3 = AnimationStates.BOSS_SKILL_3;
	private string ANIMATION_WALK = AnimationStates.BOSS_WALK;

	void Awake () {
		animator = GetComponent<Animator> ();
		playerTransform = GameObject.FindGameObjectWithTag ("Player").transform;
		playerHealth = playerTransform.gameObject.GetComponent<PlayerHealth> ();
		bossHealth = GetComponent<BossHealth> ();
	}

	void Update () {
		float distance = Vector3.Distance (transform.position, playerTransform.position);

		if (bossHealth.realHealth > 0) {
			transform.LookAt (playerTransform);
		}

		if (playerHealth.realHealth <= 0) {
			animator.SetBool (ANIMATION_SKILL_1, false);
			animator.SetBool (ANIMATION_SKILL_2, false);
			animator.SetBool (ANIMATION_SKILL_3, false);
			animator.SetBool (ANIMATION_WALK, false);
		}

		if (playerHealth.realHealth > 0) {
			if (distance > 5f) {
				animator.SetBool (ANIMATION_WALK, true);
				animator.SetBool (ANIMATION_SKILL_1, false);
				animator.SetBool (ANIMATION_SKILL_2, false);
				animator.SetBool (ANIMATION_SKILL_3, false);
			} else {
				animator.SetBool (ANIMATION_WALK, false);
				if (distance > 2.5f) {
					animator.SetBool (ANIMATION_SKILL_1, true);
					animator.SetBool (ANIMATION_SKILL_2, false);
					animator.SetBool (ANIMATION_SKILL_3, false);
				}
				if (distance <= 2.5f && distance > 0.5f) {
					animator.SetBool (ANIMATION_SKILL_1, false);
					animator.SetBool (ANIMATION_SKILL_2, true);
					animator.SetBool (ANIMATION_SKILL_3, false);
				}
				if (distance <= 0.5f) {
					animator.SetBool (ANIMATION_SKILL_1, false);
					animator.SetBool (ANIMATION_SKILL_2, false);
					animator.SetBool (ANIMATION_SKILL_3, true);
				}
			}
		}
	}
}
