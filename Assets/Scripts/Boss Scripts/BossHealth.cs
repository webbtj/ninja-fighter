﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossHealth : MonoBehaviour {

	public float realHealth;
	public AudioClip deathSound;
	private AudioSource audioSource;
	private Animator animator;

	private bool isDead;
	private CapsuleCollider collider;

	private string BASE_LAYER_DEAD = AnimationStates.BOSS_BASE_LAYER_DEAD;
	private string ANIMATION_DEAD = AnimationStates.BOSS_DEAD;

	void Awake () {
		animator = GetComponent<Animator> ();
		audioSource = GetComponent<AudioSource> ();
		collider = GetComponent<CapsuleCollider> ();
	}
	

	void Update () {
		if (isDead) {
			StopDeadAnimation();
		}
	}

	void BossDying(){
		animator.SetBool (ANIMATION_DEAD, true);
		isDead = true;
		collider.enabled = false;
		audioSource.PlayOneShot (deathSound);
	}

	public void BossTakeDamage(float amount){
		realHealth -= amount;
		if (realHealth <= 0) {
			realHealth = 0;
			BossDying ();
		}
	}

	void StopDeadAnimation(){
		if (animator.GetCurrentAnimatorStateInfo (0).IsName (BASE_LAYER_DEAD)) {
			animator.SetBool (ANIMATION_DEAD, false);
		}
	}
}
