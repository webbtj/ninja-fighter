﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OSControl : MonoBehaviour {

	public GameObject easyTouch, joystick;
	public Button[] uiButtons;
	public GameObject[] uiPanels;

	[HideInInspector]
	public static OSControl instance;

	[HideInInspector]
	public bool mobile;

	void Awake () {

		if (instance == null) {
			instance = this;
		}

		#if UNITY_EDITOR
		mobile = false;
		#elif UNITY_ANDROID
		mobile = true;
		#elif UNITY_IPHONE
		mobile = true;
		#else
		mobile = false;
		#endif

		easyTouch.SetActive (mobile);
		joystick.SetActive (mobile);
		for (int i = 0; i < uiButtons.Length; i++) {
			uiButtons [i].gameObject.SetActive (mobile);
		}
		for (int i = 0; i < uiPanels.Length; i++) {
			uiPanels [i].SetActive (mobile);
		}

	}

}
