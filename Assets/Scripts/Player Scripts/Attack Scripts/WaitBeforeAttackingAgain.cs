﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaitBeforeAttackingAgain : MonoBehaviour {

	public int waitTime;
	private int fadeTime;

	private Text waitText;
	private Image fadeImage;
	private bool canFade;
	private GameObject waitBeforeAttackingPanel;

	void Awake () {
		waitBeforeAttackingPanel = transform.GetChild (0).gameObject;

		waitText = waitBeforeAttackingPanel.GetComponentInChildren<Text> ();
		waitText.text = waitTime.ToString ();

		fadeImage = waitBeforeAttackingPanel.GetComponent<Image> ();
		fadeTime = waitTime;

		waitBeforeAttackingPanel.SetActive (false);
	}

	void Update () {
		FadeOut ();
	}

	public void ActivateFadeOut(){
		waitBeforeAttackingPanel.SetActive (true);
		waitText.text = waitTime.ToString ();

		Color temp = fadeImage.color;
		temp.a = 1f;
		fadeImage.color = temp;
		StartCoroutine(CountDown());
	}

	void FadeOut(){
		if (canFade) {
			Color temp = fadeImage.color;
			temp.a -= (Time.deltaTime / fadeTime) / 2f;
			fadeImage.color = temp;
		}
	}

	IEnumerator CountDown(){
		canFade = true;
		yield return new WaitForSeconds (1);
		waitTime -= 1;

		if (waitTime > 0) {
			waitText.text = waitTime.ToString ();
			StartCoroutine (CountDown ());
		} else {
			waitTime = fadeTime;
			waitBeforeAttackingPanel.SetActive (false);
		}
	}
}
