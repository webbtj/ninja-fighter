﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

	private Transform cameraTransform;
	private Transform target;
	public Vector3 offset = new Vector3 (3f, 7.5f, 10f);

	void Awake () {
		target = GameObject.Find ("Ninja").transform;
	}

	// Use this for initialization
	void Start () {
		cameraTransform = this.transform;
	}
	
	// Update is called once per frame
	void Update () {
		if (target) {
			cameraTransform.position = target.position + offset;
			cameraTransform.LookAt(target.position, Vector3.up);
		}
	}
}
