﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {

	public float realHealth;

	private Animator animator;
	private bool playerDead;
	private bool playerHit;

	private Slider healthSlider;
	private Text healthText;

	private BossHealth bossHealth;
	private Transform bossTransform;
	private bool victory;

	private string ANIMATION_DEAD = AnimationStates.PLAYER_DEAD;
	private string ANIMATION_ATTACK = AnimationStates.PLAYER_ATTACK;
	private string ANIMATION_VICTORY = AnimationStates.PLAYER_VICTORY;
	private string BASE_LAYER_DYING = AnimationStates.PLAYER_BASE_LAYER_DYING;
	private string BASE_LAYER_VICTORY = AnimationStates.PLAYER_BASE_LAYER_VICTORY;

	void Awake () {
		animator = GetComponent<Animator> ();

		healthSlider = GameObject.Find ("Health Foreground").GetComponent<Slider> ();
		healthText = GameObject.Find ("Health Text").GetComponent<Text> ();


		healthText.text = realHealth.ToString ();
		healthSlider.value = realHealth;

		bossTransform = GameObject.FindGameObjectWithTag ("Boss").transform;
		bossHealth = bossTransform.gameObject.GetComponent<BossHealth> ();
	}

	void Update () {
		if (realHealth <= 0) {
			realHealth = 0;
			PlayerDying();
		}
		if (playerDead) {
			StopPlayerDeadAnimation();
		}

		if (realHealth > 100) {
			realHealth = 100f;
		}

		if (bossHealth.realHealth <= 0) {
			Victory();
		}

		if (victory) {
			StopVictoryAnimation();
		}
	}

	void PlayerDying(){
		playerDead = true;
		animator.SetBool (ANIMATION_DEAD, true);
		animator.SetBool (ANIMATION_ATTACK, false);
	}

	void StopPlayerDeadAnimation(){
		if (animator.GetCurrentAnimatorStateInfo (0).IsName (BASE_LAYER_DYING)) {
			animator.SetBool (ANIMATION_DEAD, false);
		}
	}

	public void TakeDamage(float amount){
		realHealth -= amount;

		if (realHealth <= 0) {
			realHealth = 0;
		}

		if (amount > 0) {
			healthText.text = realHealth.ToString ();
			healthSlider.value = realHealth;
			playerHit = true;
		}

		Debug.Log("Player Health: " + realHealth);
	}

	void Victory(){
		animator.SetBool (ANIMATION_VICTORY, true);
		victory = true;
	}

	void StopVictoryAnimation(){
		if (animator.GetCurrentAnimatorStateInfo (0).IsName (BASE_LAYER_VICTORY)) {
			animator.SetBool (ANIMATION_VICTORY, false);
		}
	}
}
