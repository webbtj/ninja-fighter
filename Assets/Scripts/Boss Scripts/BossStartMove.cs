﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossStartMove : MonoBehaviour {

	private SphereCollider collider;
	private BossAI bossAI;

	void Awake () {
		collider = GetComponent<SphereCollider> ();
		bossAI = GameObject.FindGameObjectWithTag ("Boss").GetComponent<BossAI> ();
	}

	void OnTriggerEnter (Collider target) {
		if (target.tag == "Player") {
			bossAI.enabled = true;
			collider.enabled = false;
		}
	}
}
