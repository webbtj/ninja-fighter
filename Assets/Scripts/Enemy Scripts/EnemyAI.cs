﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour {

	private Transform zombieTransform;

	public float chaseSpeed;

	private CapsuleCollider collider;
	private Transform player;
	private Animator animator;
	private EnemyHealth enemyHealth;
	private PlayerHealth playerHealth;
	private bool victory;

	private string ANIMATION_ATTACK = AnimationStates.ENEMY_ATTACK;
	private string ANIMATION_RUN = AnimationStates.ENEMY_RUN;
	private string ANIMATION_SPEED = AnimationStates.ENEMY_SPEED;
	private string ANIMATION_VICTORY = AnimationStates.ENEMY_VICTORY;
	private string BASE_LAYER_STAND = AnimationStates.ENEMY_BASE_LAYER_STAND;

	private NavMeshAgent navAgent;
	public Transform[] navPoints;
	private int navigationIndex;

	void Awake () {
		collider = GetComponent<CapsuleCollider> ();
		animator = GetComponent<Animator> ();
		player = GameObject.FindGameObjectWithTag ("Player").transform;

		zombieTransform = this.transform;
		enemyHealth = GetComponent<EnemyHealth> ();
		playerHealth = player.gameObject.GetComponent<PlayerHealth> ();

		navAgent = GetComponent<NavMeshAgent> ();
		navigationIndex = Random.Range (0, navPoints.Length);
		navAgent.SetDestination (navPoints [navigationIndex].position);

	}

	void Update () {
		float distance = Vector3.Distance (zombieTransform.position, player.position);

//		if (enemyHealth.realHealth > 0) {
//			if (distance > 2.5f) {
//				Chase ();
//			} else {
//				Attack ();
//			}
//
//			transform.LookAt (player);
//		}

		if (enemyHealth.realHealth > 0) {
			if (distance > 7f) {
				Search ();
				navAgent.isStopped = true;
			} else {
				navAgent.isStopped = false;
				if (distance > 2.5f) {
					Chase ();
				} else {
					Attack ();
					transform.LookAt (player);
				}
			}
		}

		if (enemyHealth.realHealth <= 0) {
			collider.enabled = false;
			navAgent.isStopped = true;
		}

		if (playerHealth.realHealth <= 0) {
			EnemyVictory();
		}

		if (victory) {
			StopEnemyVictory();
		}
	}



	void Search(){
		if (navAgent.remainingDistance <= 0.5f) {
			animator.SetFloat (ANIMATION_SPEED, 0f);
			animator.SetBool (ANIMATION_ATTACK, false);
			animator.SetBool (ANIMATION_RUN, false);

			if (navigationIndex == navPoints.Length - 1) {
				navigationIndex = 0;
			} else {
				navigationIndex++;
			}
			navAgent.SetDestination (navPoints [navigationIndex].position);
		} else {
			animator.SetFloat (ANIMATION_SPEED, 1f);
			animator.SetBool (ANIMATION_ATTACK, false);
			animator.SetBool (ANIMATION_RUN, false);
		}
	}

	void Chase(){
		animator.SetBool (ANIMATION_RUN, true);
		animator.SetFloat (ANIMATION_SPEED, chaseSpeed);
		animator.SetBool (ANIMATION_ATTACK, false);
	}

	void Attack(){
		animator.SetBool (ANIMATION_ATTACK, true);
	}

	void EnemyVictory(){
		animator.SetBool (ANIMATION_VICTORY, true);
		victory = true;
	}

	void StopEnemyVictory(){
		if (animator.GetCurrentAnimatorStateInfo (0).IsName (BASE_LAYER_STAND)) {
			animator.SetBool (ANIMATION_VICTORY, false);
		}
	}
}
