﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_Damage : MonoBehaviour {

	public LayerMask zombieLayer;
	public float radius;
	public float damangeCount;
	public GameObject damageEffect;

	private bool collided;
	private EnemyHealth attackTarget;

	void Update () {
		Collider[] hits = Physics.OverlapSphere (transform.position, radius, zombieLayer);
		foreach (Collider c in hits) {
			if (c.isTrigger) {
				continue;
			}

			attackTarget = c.gameObject.GetComponent<EnemyHealth> ();
			collided = true;
		}

		if (collided) {
			collided = false;
			Instantiate (damageEffect, transform.position, transform.rotation);
			attackTarget.TakeDamage (damangeCount);
		}
	}
}
