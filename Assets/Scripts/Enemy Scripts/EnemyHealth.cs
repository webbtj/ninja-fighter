﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {

	public float realHealth;

	private AudioSource audioSource;
	public AudioClip enemyDeadSound;

	private bool enemyDead;
	private Animator animator;
	private bool enemyIsHit;

	public GameObject deathEffect;
	public Transform deathEffectPoint;

	public GameObject attackPointOne;
	public GameObject attackPointTwo;

	private string ANIMATION_ATTACK = AnimationStates.ENEMY_ATTACK;
	private string ANIMATION_BE_ATTACKED = AnimationStates.ENEMY_BE_ATTACKED;
	private string ANIMATION_DEAD = AnimationStates.ENEMY_DEAD;
	private string ANIMATION_BASE_LAYER_DYING = AnimationStates.ENEMY_BASE_LAYER_DYING;
	private string ANIMATION_BASE_LAYER_HIT = AnimationStates.ENEMY_BASE_LAYER_HIT;

	void Awake () {
		audioSource = GetComponent<AudioSource> ();
		animator = GetComponent<Animator> ();
	}

	void Update () {
		if (enemyDead) {
			StopDeadAnimation();
		}

		if (enemyIsHit) {
			EnemyAttacked();
		}

		if (!enemyIsHit) {
			StopEnemyHit();
		}
	}

	void EnemyDying(){
		animator.SetBool (ANIMATION_DEAD, true);
		animator.SetBool (ANIMATION_BE_ATTACKED, false);
		enemyDead = true;
		StartCoroutine (DeadEffect ());
		attackPointOne.SetActive(false);
		attackPointTwo.SetActive(false);
		audioSource.PlayOneShot (enemyDeadSound);
	}

	void StopDeadAnimation(){
		if (animator.GetCurrentAnimatorStateInfo (0).IsName (ANIMATION_BASE_LAYER_DYING)) {
			animator.SetBool (ANIMATION_DEAD, false);
		}
	}

	public void TakeDamage(float amount){
		realHealth -= amount;

		if (realHealth <= 0) {
			realHealth = 0;
			EnemyDying ();
		}

		if (amount > 0) {
			enemyIsHit = true;
		}
	}

	void EnemyAttacked(){
		enemyIsHit = false;
		animator.SetBool (ANIMATION_BE_ATTACKED, true);
		animator.SetBool (ANIMATION_ATTACK, false);
		transform.Translate (Vector3.back * 0.5f);
	}

	void StopEnemyHit(){
		if (animator.GetCurrentAnimatorStateInfo (0).IsName (ANIMATION_BASE_LAYER_HIT)) {
			animator.SetBool (ANIMATION_BE_ATTACKED, false);
		}
	}

	IEnumerator DeadEffect(){
		yield return new WaitForSeconds (2f);
		Instantiate (deathEffect, deathEffectPoint.position, deathEffectPoint.rotation);
		Destroy (gameObject);
	}
}
