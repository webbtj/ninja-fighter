﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveKeyboard : MonoBehaviour {

	private Animator animator;

	public FreeMovementMotor motor;
	public Transform playerTransform;

	private Quaternion screenMovementSpace;
	private Vector3 screenMovementForward;
	private Vector3 screenMovementRight;

	private string AXIS_X = "Horizontal";
	private string AXIS_Y = "Vertical";

	private bool activated;

	void Awake () {
		animator = GetComponent<Animator> ();
		motor.movementDirection = Vector2.zero;
	}

	void Start() {
		if (!OSControl.instance.mobile) {
			activated = true;
		}

		if (activated) {
			screenMovementSpace = Quaternion.Euler (0, Camera.main.transform.eulerAngles.y, 0);
			screenMovementForward = screenMovementSpace * Vector3.forward;
			screenMovementRight = screenMovementSpace * Vector3.right;
		}
	}

	void Update () {
		if (activated) {
			motor.movementDirection =
			Input.GetAxis (AXIS_X) * screenMovementRight +
			Input.GetAxis (AXIS_Y) * screenMovementForward;

			if (Input.GetAxis (AXIS_X) != 0 || Input.GetAxis (AXIS_Y) != 0) {
				animator.SetBool (AnimationStates.PLAYER_RUN, true);
			} else {
				animator.SetBool (AnimationStates.PLAYER_RUN, false);
			}

			if (motor.movementDirection.sqrMagnitude > 1) {
				motor.movementDirection.Normalize ();
			}
		}
	}
}
