﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayersAttacks : MonoBehaviour {

	public GameObject skillOneEffectPrefab;
	public GameObject skillOneDamagePrefab;

	public Transform skillOnePoint;
	public Transform skillOnePoint_1;
	public Transform skillOnePoint_2;
	public Transform skillOnePoint_3;
	public Transform skillOnePoint_4;
	public Transform skillOnePoint_5;
	public Transform skillOnePoint_6;
	public Transform skillOnePoint_7;
	public Transform skillOnePoint_8;

	public GameObject skillTwoEffectPrefab;
	public GameObject skillTwoDamagePrefab;

	public Transform skillTwoPoint;
	public Transform skillTwoPoint_1;
	public Transform skillTwoPoint_2;
	public Transform skillTwoPoint_3;
	public Transform skillTwoPoint_4;
	public Transform skillTwoPoint_5;
	public Transform skillTwoPoint_6;

	public GameObject skillThreeEffectPrefab;

	public Transform skillThreePoint_1;
	public Transform skillThreePoint_2;
	public Transform skillThreePoint_3;
	public Transform skillThreePoint_4;
	public Transform skillThreePoint_5;

	public AudioClip skillOneMusic1;
	public AudioClip skillOneMusic2;
	public AudioClip playerSkillOneSound;
	public AudioClip skillTwoMusic;
	public AudioClip skillThreeMusic;

	private Animator animator;
	private AudioSource audioSource;

	private Button skillOneButton;
	private Button skillTwoButton;
	private Button skillThreeButton;
	private Button jumpButton;

	private Rigidbody body;

	private bool s1_NotUsed;
	private bool s2_NotUsed;
	private bool s3_NotUsed;

	private string PLAYER_ATTACK = AnimationStates.PLAYER_ATTACK;
	private string PLAYER_SKILL_1 = AnimationStates.PLAYER_SKILL_1;
	private string PLAYER_SKILL_2 = AnimationStates.PLAYER_SKILL_2;
	private string PLAYER_SKILL_3 = AnimationStates.PLAYER_SKILL_3;

	void Awake () {
		animator = GetComponent<Animator> ();
		audioSource = GetComponent<AudioSource> ();
		body = GetComponent<Rigidbody> ();

		GameObject temp;

		temp = GameObject.Find ("Skill One Button");
		if (temp) {
			skillOneButton = temp.GetComponent<Button> ();
			skillOneButton.onClick.AddListener (() => SkillOneButtonPressed ());
		}

		temp = GameObject.Find ("Skill Two Button");
		if (temp) {
			skillTwoButton = temp.GetComponent<Button> ();
			skillTwoButton.onClick.AddListener (() => SkillTwoButtonPressed ());
		}

		temp = GameObject.Find ("Skill Three Button");
		if (temp) {
			skillThreeButton = temp.GetComponent<Button> ();
			skillThreeButton.onClick.AddListener (() => SkillThreeButtonPressed ());
		}

		temp = GameObject.Find ("Jump Button");
		if (temp) {
			jumpButton = temp.GetComponent<Button> ();
			jumpButton.onClick.AddListener (() => JumpButtonPressed ());
		}

		s1_NotUsed = true;
		s2_NotUsed = true;
		s3_NotUsed = true;
	}

	void Update () {
		HandleButtonPresses ();
	}

	//Button Listeners

	public void AttackButtonPressed(){
		animator.SetBool (PLAYER_ATTACK, true);
	}

	public void AttackButtonReleased(){
		animator.SetBool (PLAYER_ATTACK, false);
	}

	public void SkillOneButtonPressed(){
		animator.SetBool (PLAYER_SKILL_1, true);
	}

	public void SkillTwoButtonPressed(){
		animator.SetBool (PLAYER_SKILL_2, true);
	}

	public void SkillThreeButtonPressed(){
		animator.SetBool (PLAYER_SKILL_3, true);
	}

	public void JumpButtonPressed(){
		if (transform.position.y < 1) {
			body.AddForce (new Vector3 (0, 2f, 0), ForceMode.Impulse);
		}
	}

	void HandleButtonPresses () {
		if (Input.GetKeyDown (KeyCode.I)) {
			animator.SetBool (PLAYER_ATTACK, true);
		}

		if (Input.GetKeyUp (KeyCode.I)) {
			animator.SetBool (PLAYER_ATTACK, false);
		}

		if (Input.GetKeyDown (KeyCode.J)) {
			if (s1_NotUsed) {
				s1_NotUsed = false;
				animator.SetBool (PLAYER_SKILL_1, true);
				StartCoroutine (ResetSkills (1));
			}
		}

		if (Input.GetKeyDown (KeyCode.K)) {
			if (s2_NotUsed) {
				s2_NotUsed = false;
				animator.SetBool (PLAYER_SKILL_2, true);
				StartCoroutine (ResetSkills (2));
			}
		}

		if (Input.GetKeyDown (KeyCode.L)) {
			if (s3_NotUsed) {
				s3_NotUsed = false;
				animator.SetBool (PLAYER_SKILL_3, true);
				StartCoroutine (ResetSkills (3));
			}
		}
	}

	//Skill One Events

	void SkillOne(bool skillOne){
		if (skillOne) {
			Instantiate (skillOneEffectPrefab, skillOnePoint.transform.position, skillOnePoint.transform.rotation);
			audioSource.PlayOneShot (skillOneMusic1);
			StartCoroutine (SkillOneCoroutine ());
		}
	}

	void SkillOneSound(bool play){
		if (play) {
			audioSource.PlayOneShot (playerSkillOneSound);
		}
	}

	void SkillOneEnd(bool skillOneEnd){
		if (skillOneEnd) {
			animator.SetBool (PLAYER_SKILL_1, false);
		}
	}

	IEnumerator SkillOneCoroutine(){
		yield return new WaitForSeconds (1.5f);
		audioSource.PlayOneShot (skillOneMusic2);
		Instantiate (skillOneDamagePrefab, skillOnePoint_1.position, skillOnePoint_1.rotation);
		Instantiate (skillOneDamagePrefab, skillOnePoint_2.position, skillOnePoint_2.rotation);
		Instantiate (skillOneDamagePrefab, skillOnePoint_3.position, skillOnePoint_3.rotation);
		Instantiate (skillOneDamagePrefab, skillOnePoint_4.position, skillOnePoint_4.rotation);
		Instantiate (skillOneDamagePrefab, skillOnePoint_5.position, skillOnePoint_5.rotation);
		Instantiate (skillOneDamagePrefab, skillOnePoint_6.position, skillOnePoint_6.rotation);
		Instantiate (skillOneDamagePrefab, skillOnePoint_7.position, skillOnePoint_7.rotation);
		Instantiate (skillOneDamagePrefab, skillOnePoint_8.position, skillOnePoint_8.rotation);
	}

	//Skill Two Events

	void SkillTwo(bool skillTwo){
		if (skillTwo) {
			Instantiate (skillTwoEffectPrefab, skillTwoPoint.transform.position, skillTwoPoint.transform.rotation);
			audioSource.PlayOneShot (skillTwoMusic);
			StartCoroutine (SkillTwoCoroutine ());
		}
	}

	void SkillTwoEnd(bool skillTwoEnd){
		if (skillTwoEnd) {
			animator.SetBool (PLAYER_SKILL_2, false);
		}
	}

	IEnumerator SkillTwoCoroutine(){
		yield return new WaitForSeconds (1.5f);
		Instantiate (skillTwoDamagePrefab, skillTwoPoint_1.position, skillTwoPoint_1.rotation);
		Instantiate (skillTwoDamagePrefab, skillTwoPoint_2.position, skillTwoPoint_2.rotation);
		Instantiate (skillTwoDamagePrefab, skillTwoPoint_3.position, skillTwoPoint_3.rotation);
		Instantiate (skillTwoDamagePrefab, skillTwoPoint_4.position, skillTwoPoint_4.rotation);
		Instantiate (skillTwoDamagePrefab, skillTwoPoint_5.position, skillTwoPoint_5.rotation);
		Instantiate (skillTwoDamagePrefab, skillTwoPoint_6.position, skillTwoPoint_6.rotation);
	}

	//Skill Three Events
	void SkillThree(bool skillThree){
		if (skillThree) {
			Instantiate (skillThreeEffectPrefab, skillThreePoint_1.transform.position, skillThreePoint_1.transform.rotation);
			Instantiate (skillThreeEffectPrefab, skillThreePoint_2.transform.position, skillThreePoint_2.transform.rotation);
			Instantiate (skillThreeEffectPrefab, skillThreePoint_3.transform.position, skillThreePoint_3.transform.rotation);
			Instantiate (skillThreeEffectPrefab, skillThreePoint_4.transform.position, skillThreePoint_4.transform.rotation);
			Instantiate (skillThreeEffectPrefab, skillThreePoint_5.transform.position, skillThreePoint_5.transform.rotation);
		}
	}

	void SkillThreeEnd(bool skillThreeEnd){
		if (skillThreeEnd) {
			animator.SetBool (PLAYER_SKILL_3, false);
		}
	}

	IEnumerator ResetSkills(int skill){
		yield return new WaitForSeconds (3f);

		switch (skill) {
		case 1:
			s1_NotUsed = true;
			break;
		case 2:
			s2_NotUsed = true;
			break;
		case 3:
			s3_NotUsed = true;
			break;
		}

	}
}
