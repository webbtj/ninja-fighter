﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeMovementMotor : MonoBehaviour {

	[HideInInspector]
	public Vector3 movementDirection;

	private Rigidbody body;

	public float walkingSpeed = 5f;
	public float walkingSnap = 50f;
	public float turningSmoothing = 0.3f;

	private bool activated;

	void Awake () {
		body = GetComponent<Rigidbody> ();
	}

	void Start () {
		if (!OSControl.instance.mobile) {
			activated = true;
		}
	}

	void FixedUpdate () {
		
		if (activated) {
			
			Vector3 targetVelocity = movementDirection * walkingSpeed;
			Vector3 deltaVelocity = targetVelocity - body.velocity;

			if (body.useGravity) {
				deltaVelocity.y = 0f;
			}

			body.AddForce (deltaVelocity * walkingSnap, ForceMode.Acceleration);

			Vector3 faceDirection = movementDirection;

			if (faceDirection == Vector3.zero) {
				body.angularVelocity = Vector3.zero;
			} else {
				float rotationAngle = AngleAroundAxis (transform.forward, faceDirection, Vector3.up);
				body.angularVelocity = (Vector3.up * rotationAngle * turningSmoothing);
			}

			if (Input.GetKeyDown (KeyCode.Space) && transform.position.y < 1) {
				body.AddForce (new Vector3 (0, 2f, 0), ForceMode.Impulse);
			}

		}

	}

	static float AngleAroundAxis(Vector3 directionA, Vector3 directionB, Vector3 axis) {
		directionA = directionA - Vector3.Project (directionA, axis);
		directionB = directionB - Vector3.Project (directionB, axis);

		float angle = Vector3.Angle (directionA, directionB);
		return angle * (Vector3.Dot(axis, Vector3.Cross(directionA, directionB)) < 0 ? -1 : 1);

	}
}
