﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveJoystick : MonoBehaviour {

	private Transform playerTransform;
	private Animator animator;

	private AudioSource audioSource;
	public AudioClip footStep1;
	public AudioClip footStep2;

	private string PLAYER_RUN = AnimationStates.PLAYER_RUN;

	void Awake () {
		animator = GetComponent<Animator> ();
		audioSource = GetComponent<AudioSource> ();
		playerTransform = this.transform;
	}

	void OnEnable(){
		EasyJoystick.On_JoystickMove += JoystickMove;
		EasyJoystick.On_JoystickMoveEnd += JoystickMoveEnd;
	}

	void OnDisable(){
		EasyJoystick.On_JoystickMove -= JoystickMove;
		EasyJoystick.On_JoystickMoveEnd -= JoystickMoveEnd;
	}

	void JoystickMove(MovingJoystick move){
		float angle = move.Axis2Angle (true);
		playerTransform.rotation = Quaternion.Euler(new Vector3(0, angle - 45, 0));
		animator.SetBool (PLAYER_RUN, true);
	}

	void JoystickMoveEnd(MovingJoystick move){
		animator.SetBool (PLAYER_RUN, false);
	}

	void FootStepOne(bool play){
		if (play) {
			audioSource.PlayOneShot (footStep1);
		}
	}

	void FootStepTwo(bool play){
		if (play) {
			audioSource.PlayOneShot (footStep2);
		}
	}
}
